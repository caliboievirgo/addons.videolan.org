<?php
namespace Addons\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

class AdminController extends AbstractActionController
{
    protected $addonsTable;
    protected $addonsFileTable;
    protected $navigation;

    public function updateNavLabels( $currentname )
    {
        foreach( array('inspect') as $id )
        {
            $page = $this->navigation->findBy('id', $id);
            if ( $page )
            {
                $page->setLabel( str_replace( '%1', htmlspecialchars($currentname), $page->getLabel() ) );
            }
        }
    }

    public function onDispatch(MvcEvent $e)
    {
        $this->navigation = $this->getServiceLocator()->get('navigation');

        if ( $this->params('uuid') )
        {
            $page = $this->navigation->findBy('id', 'AdminRoot');

            $detailspage = new \Zend\Navigation\Page\Mvc(
                    array(
                        'id' => 'inspect',
                        'label' => '"%1"',
                        'route' => 'admin/inspect',
                        'params' => array('uuid' => $this->params('uuid') )
                    )
            );
            $detailspage->setRouter( $e->getRouter() );
            $detailspage->setRouteMatch( $e->getRouteMatch() );

            $page->addPage( $detailspage );
        }

        return parent::onDispatch($e);
    }

    public function inspectAction()
    {
        if ( !$this->addonsTable || !$this->addonsFileTable )
        {
            $sm = $this->getServiceLocator();
            if ( !$this->addonsTable )
                $this->addonsTable = $sm->get('Addons\Model\AddonsTable');
            if ( !$this->addonsFileTable )
                $this->addonsFileTable = $sm->get('Addons\Model\AddonsFileTable');
        }

        $addon = $this->addonsTable->getAddon( $this->params('uuid') );

        //$this->checkOwner($this->params('uuid'), $addon);

        $this->updateNavLabels( $addon->name );

        $addonfiles = $this->addonsFileTable->fetch( $addon->uuid );

        $zfcserv = $this->getServiceLocator()->get('zfcuser_user_service');
        $mapper = $zfcserv->getUserMapper();
        $user = $mapper->findById( $addon->ref_userid );
        if ( empty($user) )
            $user = new \ZfcUser\Entity\User();

        return array('addon' => $addon, 'addonfiles' => $addonfiles, 'user' => $user );
    }

    public function indexAction()
    {
        if ( !$this->addonsTable )
        {
            $sm = $this->getServiceLocator();
            $this->addonsTable = $sm->get('Addons\Model\AddonsTable');
        }

        $paginator = $this->addonsTable->fetch( $this->params('type'), $this->params('order'), $this->params()->fromQuery('lookup', null), true, $this->params('state') );
        $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));

        $paginator->setItemCountPerPage( ( $this->params('type') == "skin" ) ? 16 : 10 );

        return new ViewModel( array( 'paginator' => $paginator,
                                     'addons' => $paginator,
                                     'type' => $this->params('type'),
                                     'state' => $this->params('state') ) );

    }

}
